from setuptools import setup

setup(
    name='VnVExtractor',
    version='1.0.0',
    packages=[''],
    url='https://bitbucket.org/togo/vnvextractor',
    license='MIT License',
    author='Daniel Gehriger',
    author_email='gehriger@gmail.com',
    description='Script for extracting traceability information from tables inside Word documents',
    install_requires=['Unidecode', 'argparse', 'typing'],
    python_requires=">=3.3"
)
