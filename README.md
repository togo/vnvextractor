# VnVExtractor

Script for extracting traceability information from tables inside Word documents.

## Description

This script takes as its input the path to a Microsoft Word document, or a wildcard expression specifying a set of Word documents, and extracts tracability information from tables contained in those documents.

The tables are expected to have a *source* and a *target* column, where the *source* column contains an identifier (e.g. a test-ID), and the *target* column a related identifier, or list of identifiers (e.g. uptraces to requirement IDs).

### Column Headers
By default, the *source* column shall have the header text "Test ID", and the *target* column the header text "Uptrace". This can be changed using the optional arguments `--source` and `--target`.

### Identifiers

The script uses regular expressions to detect *source* and *target* identifiers. 

By default, *source* identifiers are matched using regex `[A-Z]+\d+\s*-\S+(?:\s*\([^)]+\))?`, which matches a series of characters, a number, a dash, and optionally an expression in round brackets. Example: `VER004-0115(30101-32002)`. Matching is not case sensitive. The regex can be changed using `--source-regex`.

By default, the *target* identifier is matched using the regex `[A-Z]+\s*-\s*\d+[A-Z]?`, which matches a series of characters, a dash, a number and optionally a trailing character (whitespace is allowed around the dash). Example: `SWDS-1234S`. Matching is not case sensitive. The regex can be changed using `--target-regex`.

### Uptrace vs Downtrace

The script will list *source* identifiers in the first column, and the corresponding `target` identifier in the second column. The tracability matrix may be **inverted** using the `--downtrace` option, which lists `target` identifiers in the first column and `source` identifiers in the 2nd column.

If an identifier in the first column traces to several items in the 2nd column, each will be listed on a separate line, with the identifier in the 1st column repeated. Using `--list` (e.g. `--list=","`), multiple *target* IDs will be listed on a single row, separated by the specified string. 

*Example: *

	VER005-0200	SWDS-3350S,SWDS-3310,SWDS-3460S,SWDS-3462S,SWDS-3465S,SWDS-3463S,SWDS-6030S,SWDS-3461S

### Other Options

 - `--separator` can be used to specify the column separator (defaults to tab character)
 - `--skip-empty-target` can be used to omit listing identifiers that do not trace to and *target*.
 - `--verbose` increases verbosity

### Sample Usage

    python VnVExtractor.py --skip-empty-target --verbose --list=", " --target-regex="(?:SWDS\s*-\s*\d+[A-Z]?)" "6204.9000.*.docx"

Output:

    VER004-0105(10101-13305)        SWDS-6020S
    VER004-0105(10701-10732)        SWDS-3450S
    VER004-0105(12101-12109)        SWDS-3580S
    VER004-0110(20101-21901)        SWDS-6020S
    VER004-0110(20601-20612)        SWDS-3450S
    VER004-0110(21401-21403)        SWDS-1351S
    VER004-0110(21901)      		SWDS-3451S
    VER004-0115(30101-32002)        SWDS-6020S
    VER004-0120(40101-43901)        SWDS-6020S
    VER004-0125(50101-50103)        SWDS-3351S
    VER004-0125(50101-50402)        SWDS-6020S
    VER004-0130(60201-60401)        SWDS-6020S
    VER004-0135(70101)      		SWDS-5300S
    VER004-0135(70101-70802)        SWDS-6020S
    VER004-0135(70201)      		SWDS-1312, SWDS-1311
    VER004-0140     				SWDS-6020S
    VER004-0145(90101-90102)        SWDS-2400S, SWDS-3600
    VER004-0145(90101-90201)        SWDS-6020S
    VER004-0145(90201-90202)        SWDS-5710S, SWDS-5720S
    VER004-0205     				SWDS-6010, SWDS-6011S    

## Usage

    python VnVExtractor.py [-h] [-u | -d] [-l LIST] [-s SOURCE] [-t TARGET]
       [-c SEPARATOR] [-e] [--target-regex TARGET_REGEX]
       [--source-regex SOURCE_REGEX] [-v]
       input_path
    
    positional arguments:
      input_path            Word files to parse (glob)
    
    optional arguments:
      -h, --help            show this help message and exit
      -u, --uptrace         output uptrace (source to target)
      -d, --downtrace       output downtrace (target to source)
      -l LIST, --list LIST  list multiple trace destinations on same line,
                            separated by specified character
      -s SOURCE, --source SOURCE
                            column header for source IDs (default: "Test ID")
      -t TARGET, --target TARGET
                            column header for target IDs (default: "Uptrace")
      -c SEPARATOR, --separator SEPARATOR
                            column separator (default: tab)
      -e, --skip-empty-target
                            column separator (default: tab)
      --target-regex TARGET_REGEX
                            regular expression for target ID (default:
                            [A-Z]+\s*-\s*\d+[A-Z]?
      --source-regex SOURCE_REGEX
                            regular expression for source ID (default:
                            [A-Z]+\d+\s*-\S+(?:\s*\([^)]+\))?
      -v, --verbose         increases log verbosity for each occurrence.

## 