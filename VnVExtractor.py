import argparse
import glob
import logging
import ntpath
import os
import re
import sys
from collections import defaultdict

import unidecode
import win32com.client as win32
from typing import Set, Dict, List

if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

module = sys.modules['__main__'].__file__
log = logging.getLogger(module)


class TracabilityTable:

    def __init__(self, table, src_idx, tgt_idx, args):
        self.tracability = defaultdict(set)
        self.re_src = re.compile("({})".format(args.source_regex), re.IGNORECASE)
        self.re_tgt = re.compile("({})".format(args.target_regex), re.IGNORECASE)

        for row in range(2, table.Rows.Count + 1):
            try:
                tgt_text = self.sanitize_text(self.get_cell_text(table, row, tgt_idx))
                src_text = self.sanitize_text(self.get_cell_text(table, row, src_idx))

                test_id = self.parse_source_id(src_text)
                if test_id is None:
                    continue

                self.tracability[test_id] |= set(self.parse_target_ids(tgt_text))
            except ValueError:
                # Cell
                pass


    @staticmethod
    def sanitize_text(text: str) -> str:
        """Clean text by converting dashes-likes to dashes, and stripping whitespace

        :param text: input string
        :return: cleaned string
        """
        ascii_text = unidecode.unidecode(text.strip('\a\t\n\r'))
        return ascii_text.translate(str.maketrans('\r\x1e\x96\x97', ' ---'))

    @staticmethod
    def get_cell_text(table: object, row: int, col: int) -> str:
        """Get table cell text

        :param table: Word table
        :param row: Row index
        :param col: Column index
        :return: Cell text
        """
        try:
            return table.Cell(Row=row, Column=col).Range.Text
        except Exception:
            for c in table.Range.Cells:
                if c.RowIndex == row and c.ColumnIndex == col:
                    return c.Text
            raise ValueError('Cell not found')

    def parse_target_ids(self, id_text: str) -> List[str]:
        """Extract target IDs from string

        :param id_text: String
        :return: List of target IDs
        """
        match = self.re_tgt.findall(id_text)
        if match is not None:
            result = []
            for u in match:
                if u is not None:
                    result.append("".join(u.split()))
            return result
        else:
            return []

    def parse_source_id(self, id_text: str) -> str:
        """Extract source ID from string

        :param id_text: String
        :return: Source ID
        """
        match = self.re_src.match(id_text)
        if match is not None:
            result = match.group(1)
            return "".join(result.split())
        else:
            return None


class WordDoc:
    wdDoNotSaveChanges = 0

    def __init__(self, path, args):
        self.args = args
        word = win32.Dispatch("Word.Application")
        word.Visible = 0
        word.Documents.Open(path)
        self.doc = word.ActiveDocument

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.doc.Close(SaveChanges=0)

    @staticmethod
    def get_column_indices(table: object) -> Dict[str, int]:
        """Return dictionary of column headers mapped to their their column indices

        :param table: Word table
        :return: Dictionary mapping column headers to indices
        """
        result = {}

        try:
            for col in range(1, table.Columns.Count + 1):
                text = table.Cell(Row=1, Column=col).Range.Text.strip(' \t\n\r\a').lower()
                result[text] = col
        finally:
            return result

    def get_tables(self) -> TracabilityTable:
        """Enumerates tables in document
        """
        for table in self.doc.Tables:
            col_idx = self.get_column_indices(table)
            if self.args.source.lower() in col_idx and self.args.target.lower() in col_idx:
                src_idx = col_idx[self.args.source.lower()]
                tgt_idx = col_idx[self.args.target.lower()]
                yield TracabilityTable(table, src_idx, tgt_idx, self.args)


def main():
    try:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG,
                            format='%(name)s (%(levelname)s): %(message)s')

        args = parse_cmdline()

        log.setLevel(max(3 - args.verbose_count, 0) * 10)

        if args.downtrace:
            args.uptrace = False
        if args.uptrace:
            log.info("Generating uptrace")
        else:
            log.info("Generating downtrace")

        # Glob input path
        proto_files = glob.glob(os.path.abspath(args.input_path))
        tracability = defaultdict(set)

        # Parse documents
        for proto_file in proto_files:
            log.info('Extracting data from "{}"'.format(ntpath.basename(proto_file)))

            with WordDoc(proto_file, args) as doc:
                # Parse tables in Word document
                for table in doc.get_tables():
                    local_tracability = table.tracability

                    # Combine into tracability matrix
                    for src_id in local_tracability.keys():
                        tracability[src_id] |= local_tracability[src_id]

        # Output result
        if args.uptrace:
            output_tracability(args, tracability)
        else:
            output_tracability(args, invert_matrix(tracability))

    except Exception as e:
        logging.exception("message")


def parse_cmdline() -> object:
    """Parses the command line

    :return: Command line arguments
    """
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-u", "--uptrace", help="output uptrace (source to target)", action="store_true",
                       default=True)
    group.add_argument("-d", "--downtrace", help="output downtrace (target to source)", action="store_true")
    parser.add_argument("-l", "--list", default="", help="list multiple trace destinations on same line, separated "
                                                         "by specified character")
    parser.add_argument("-s", "--source", help="column header for source IDs (default: \"Test ID\")",
                        default="Test ID")
    parser.add_argument("-t", "--target", help="column header for target IDs (default: \"Uptrace\")",
                        default="Uptrace")
    parser.add_argument("-c", "--separator", default="\t", help="column separator (default: tab)")
    parser.add_argument("-e", "--skip-empty-target", action="store_true", help="column separator (default: tab)")
    parser.add_argument("--target-regex", default=r"[A-Z]+\s*-\s*\d+[A-Z]?",
                        help=r"regular expression for target ID (default: [A-Z]+\s*-\s*\d+[A-Z]?")
    parser.add_argument("--source-regex", default=r"[A-Z]+\d+\s*-\S+(?:\s*\([^)]+\))?",
                        help=r"regular expression for source ID (default: [A-Z]+\d+\s*-\S+(?:\s*\([^)]+\))?")
    parser.add_argument("-v", "--verbose", dest="verbose_count",
                        action="count", default=0,
                        help="increases log verbosity for each occurrence.")
    parser.add_argument("input_path", help="Word files to parse (glob)")
    args = parser.parse_args()
    return args


def invert_matrix(traceability: Dict[str, Set[str]]) -> Dict[str, Set[str]]:
    """Inverts tracability matrix

    :param traceability: Input matrix
    :return: Inverted matrix
    """
    inverse_matrix = defaultdict(set)
    for src_id, tgt_ids in traceability.items():
        for tgt_id in tgt_ids:
            inverse_matrix[tgt_id].add(src_id)
    return inverse_matrix


def output_tracability(args: object, tracability: Dict[str, Set[str]]) -> None:
    for src_id in sorted(tracability.keys()):
        tgt_ids = tracability[src_id]
        if not args.skip_empty_target or tgt_ids:
            if args.list == "":
                for tgt_id in tgt_ids:
                    print("{}{}{}".format(src_id, args.separator, tgt_id))
            else:
                print("{}{}{}".format(src_id, args.separator, args.list.join(tgt_ids)))


if __name__ == '__main__':
    main()
